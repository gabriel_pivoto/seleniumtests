from selenium import webdriver
from selenium.webdriver import Keys
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.edge.service import Service
import time

service = Service(EdgeChromiumDriverManager().install())

navegador = webdriver.Edge(service=service)

navegador.get('https://www.python.org/')

navegador.find_element('xpath','//*[@id="documentation"]/a').click()
time.sleep(5)

navegador.find_element('xpath','//*[@id="id-search-field"]').send_keys('Classes')
navegador.find_element('xpath','//*[@id="submit"]').click()
time.sleep(10)